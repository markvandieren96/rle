use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize)]
pub struct Run<T> {
	pub length: usize,
	pub value: T,
}

impl<T> std::fmt::Display for Run<T>
where
	T: std::fmt::Display,
{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let s = format!("{}", self.value).repeat(self.length);
		write!(f, "{}", s)
	}
}

impl<T> std::fmt::Debug for Run<T>
where
	T: std::fmt::Debug,
{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}x{:?}", self.length, self.value)
	}
}
