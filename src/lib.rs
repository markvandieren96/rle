mod run;

use run::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize)]
pub struct Rle<T> {
	runs: Vec<Run<T>>,
}

impl<T> Default for Rle<T> {
	fn default() -> Self {
		Self { runs: Vec::new() }
	}
}

impl<T> Rle<T> {
	pub fn new() -> Self {
		Self::default()
	}

	pub fn set(&mut self, index: usize, value: T)
	where
		T: std::fmt::Display + PartialEq + Clone,
	{
		let mut start = 0;
		for i in 0..self.runs.len() {
			let end = start + self.runs[i].length;

			if index >= start && index < end {
				if self.runs[i].value == value {
					return;
				}
				let before = index - start;
				let after = end - (index + 1);

				if before == 0 {
					self.runs.insert(
						i + 1,
						Run {
							length: after,
							value: self.runs[i].value.clone(),
						},
					);
					self.runs[i] = Run { length: 1, value };
				} else if after == 0 {
					self.runs[i].length = before;
					self.runs.insert(i + 1, Run { length: 1, value });
				} else {
					self.runs[i].length = before;
					self.runs.insert(i + 1, Run { length: 1, value });
					self.runs.insert(
						i + 2,
						Run {
							length: after,
							value: self.runs[i].value.clone(),
						},
					);
				}

				return;
			} else {
				start += self.runs[i].length;
			}
		}

		self.push(value);
	}

	pub fn push(&mut self, value: T)
	where
		T: std::fmt::Display + PartialEq,
	{
		if let Some(run) = self.runs.last_mut() {
			if run.value == value {
				run.length += 1;
				return;
			}
		}
		self.runs.push(Run { length: 1, value });
	}

	pub fn push_n(&mut self, value: T, num: usize)
	where
		T: std::fmt::Display + PartialEq,
	{
		if let Some(run) = self.runs.last_mut() {
			if run.value == value {
				run.length += num;
				return;
			}
		}
		self.runs.push(Run { length: num, value });
	}

	pub fn get(&self, index: usize) -> Option<&T>
	where
		T: std::fmt::Display,
	{
		let mut start = 0;
		for run in self.runs.iter() {
			let end = start + run.length;
			if index >= start && index < end {
				return Some(&run.value);
			} else {
				start += run.length;
			}
		}
		None
	}

	pub fn decompress(&self) -> Vec<T>
	where
		T: Clone,
	{
		let mut out = Vec::new();
		for run in self.runs.iter() {
			for _ in 0..run.length {
				out.push(run.value.clone());
			}
		}
		out
	}

	pub fn count_runs(&self) -> usize {
		self.runs.len()
	}

	pub fn count_max_run_length(&self) -> usize {
		let mut max = 0;
		for run in self.runs.iter() {
			if run.length > max {
				max = run.length;
			}
		}
		max
	}

	pub fn len(&self) -> usize {
		self.runs.iter().map(|element| element.length).sum()
	}

	pub fn is_empty(&self) -> bool {
		self.len() == 0
	}

	pub fn iter(&self) -> impl Iterator<Item = &T> {
		self.runs
			.iter()
			.map(|run| std::iter::repeat(&run.value).take(run.length))
			.flatten()
		//self.runs.iter().map(|run| &run.value)
	}

	pub fn iter_runs(&self) -> impl Iterator<Item = &Run<T>> {
		self.runs.iter()
	}
}

impl<T> std::fmt::Display for Rle<T>
where
	T: std::fmt::Display,
{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let runs: Vec<String> = self
			.runs
			.iter()
			.map(|element| format!("{}", element))
			.collect();
		write!(f, "[{}]", runs.join(""))
	}
}

impl<T> std::fmt::Debug for Rle<T>
where
	T: std::fmt::Debug,
{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let runs: Vec<String> = self
			.runs
			.iter()
			.map(|element| format!("{:?}", element))
			.collect();
		write!(f, "[{}]", runs.join(", "))
	}
}

impl<T, I> std::convert::From<I> for Rle<T>
where
	T: std::fmt::Display + PartialEq,
	I: Iterator<Item = T>,
{
	fn from(iter: I) -> Rle<T> {
		let mut out = Rle::new();
		for t in iter {
			out.push(t);
		}
		out
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_rle_push1() {
		let mut v: Rle<char> = Rle::new();
		v.push('a');
		v.push('b');
		v.push('c');

		println!("{}: {:?}", v, v);

		assert_eq!(*v.get(0).unwrap(), 'a');
		assert_eq!(*v.get(1).unwrap(), 'b');
		assert_eq!(*v.get(2).unwrap(), 'c');
		assert_eq!(v.len(), 3);
		assert_eq!(v.count_runs(), 3);
	}

	#[test]
	fn test_rle_push2() {
		let mut v: Rle<char> = Rle::new();
		v.push('a');
		v.push('b');
		v.push('b');
		v.push('c');

		println!("{}: {:?}", v, v);

		assert_eq!(*v.get(0).unwrap(), 'a');
		assert_eq!(*v.get(1).unwrap(), 'b');
		assert_eq!(*v.get(2).unwrap(), 'b');
		assert_eq!(*v.get(3).unwrap(), 'c');
		assert_eq!(v.len(), 4);
		assert_eq!(v.count_runs(), 3);
	}

	#[test]
	fn test_rle_set1() {
		let mut v: Rle<char> = Rle::new();
		v.set(0, 'a');
		v.set(1, 'b');
		v.set(2, 'c');

		println!("{}: {:?}", v, v);

		assert_eq!(*v.get(0).unwrap(), 'a');
		assert_eq!(*v.get(1).unwrap(), 'b');
		assert_eq!(*v.get(2).unwrap(), 'c');
		assert_eq!(v.len(), 3);
		assert_eq!(v.count_runs(), 3);
	}

	#[test]
	fn test_rle_set2() {
		let mut v: Rle<char> = Rle::new();
		v.set(0, 'a');
		v.set(1, 'b');
		v.set(2, 'c');
		v.set(3, 'c');

		println!("{}: {:?}", v, v);

		assert_eq!(*v.get(0).unwrap(), 'a');
		assert_eq!(*v.get(1).unwrap(), 'b');
		assert_eq!(*v.get(2).unwrap(), 'c');
		assert_eq!(*v.get(3).unwrap(), 'c');
		assert_eq!(v.len(), 4);
		assert_eq!(v.count_runs(), 3);
	}

	#[test]
	fn test_rle_set3() {
		let mut v: Rle<char> = Rle::new();
		v.push('a');
		v.push('a');
		v.push('a');
		v.push('a');

		println!("{}: {:?}", v, v);

		v.set(1, 'b');

		println!("{}: {:?}", v, v);

		assert_eq!(*v.get(0).unwrap(), 'a');
		assert_eq!(*v.get(1).unwrap(), 'b');
		assert_eq!(*v.get(2).unwrap(), 'a');
		assert_eq!(*v.get(3).unwrap(), 'a');
		assert_eq!(v.len(), 4);
		assert_eq!(v.count_runs(), 3);
	}

	#[test]
	fn test_rle_set4() {
		let mut v: Rle<char> = Rle::new();
		v.push('a');
		v.push('a');
		v.push('a');
		v.push('a');

		println!("{}: {:?}", v, v);

		v.set(0, 'b');

		println!("{}: {:?}", v, v);

		assert_eq!(*v.get(0).unwrap(), 'b');
		assert_eq!(*v.get(1).unwrap(), 'a');
		assert_eq!(*v.get(2).unwrap(), 'a');
		assert_eq!(*v.get(3).unwrap(), 'a');
		assert_eq!(v.len(), 4);
		assert_eq!(v.count_runs(), 2);
	}

	#[test]
	fn test_rle_set5() {
		let mut v: Rle<char> = Rle::new();
		v.push('a');
		v.push('a');
		v.push('a');
		v.push('a');

		println!("{}: {:?}", v, v);

		v.set(3, 'b');

		println!("{}: {:?}", v, v);

		assert_eq!(*v.get(0).unwrap(), 'a');
		assert_eq!(*v.get(1).unwrap(), 'a');
		assert_eq!(*v.get(2).unwrap(), 'a');
		assert_eq!(*v.get(3).unwrap(), 'b');
		assert_eq!(v.len(), 4);
		assert_eq!(v.count_runs(), 2);
	}

	#[test]
	fn test_rle_set6() {
		let mut v: Rle<char> = Rle::new();
		v.push('a');
		v.push('a');
		v.push('b');
		v.push('b');

		println!("{}: {:?}", v, v);

		v.set(2, 'a');

		println!("{}: {:?}", v, v);

		assert_eq!(*v.get(0).unwrap(), 'a');
		assert_eq!(*v.get(1).unwrap(), 'a');
		assert_eq!(*v.get(2).unwrap(), 'a');
		assert_eq!(*v.get(3).unwrap(), 'b');
		assert_eq!(v.len(), 4);
		assert_eq!(v.count_runs(), 2);
	}

	#[test]
	fn test_rle_iter() {
		let mut v: Rle<char> = Rle::new();
		v.push('a');
		v.push('b');
		v.push('b');
		v.push('c');

		let mut iter = v.iter();

		println!("{}: {:?}", v, v);

		assert_eq!(*iter.next().unwrap(), 'a');
		assert_eq!(*iter.next().unwrap(), 'b');
		assert_eq!(*iter.next().unwrap(), 'b');
		assert_eq!(*iter.next().unwrap(), 'c');
		assert_eq!(iter.next(), None);
	}
}
